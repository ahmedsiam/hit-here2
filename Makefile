prefix = /usr/local
CFLAGS = `dpkg-buildflags --get CFLAGS`
CFLAGS += `dpkg-buildflags --get CPPFLAGS`
LDFLAGS = `dpkg-buildflags --get LDFLAGS`

all:
	$(CC) $(CFLAGS) -o hithere hithere.c $(LDFLAGS)

install:
	install hithere $(DESTDIR)$(prefix)/bin
	install hithere.1 $(DESTDIR)$(prefix)/share/man/man1

clean:
	rm -f hithere
